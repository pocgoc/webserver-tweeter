package controllers;

import play.*;
import play.mvc.*;
import groovy.ui.text.FindReplaceUtility;

import java.text.SimpleDateFormat;
import java.util.*;

import models.*;

public class FollowController extends Controller
{
	public  String uuid; //mandates use of getter method getId()

	public static void index()
	{
		User user = Accounts.getCurrentUser();
		Logger.info("Tweet ctrler : user is " + user.email);
		List<User> userall = User.findAll();
		Logger.info("Tweet ctrler : user is " + userall);
		List<User> ListUsers = User.findAll();
		List<User> allUsers = new ArrayList();
		Logger.info("Tweet ctrler : user is " + allUsers);
		for (User users : ListUsers)
		{
			if (users != user)
			{
				Logger.info("If Condidtion " + user.email);
				Logger.info("If Condidtion " + users.email);

				allUsers.add(users);
			}
			else
			{
				Logger.info("Tweet ctrler : user is " + "ignored");

			}
		
		}
		render(user, allUsers);
	}

	public static void follow(String Followid, String uuid)
	{
		User user = Accounts.getCurrentUser();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{

			User Followuser = User.findById(uuid);
			if (user != Followuser)
			{
				//String userId = session.get("logged_in_userid");
				Logger.info(user.firstName + " is following: " + Followuser.firstName  + " " + Followuser.lastName);


				user.befriend(Followid, Followuser);
				index();
			}
			else
			{
				index();
			}
		}
	}

	public static void unfollow(String uuid)
	{
		User user = Accounts.getCurrentUser();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{

			User unFollowuser = User.findById(uuid);
			//User me = User.findById(Long.parseLong(userId));
			if (user != unFollowuser)
			{
				user.unfriend(unFollowuser);
				Logger.info("Dropping " + unFollowuser.firstName);
				index();
			}
			else
			{
				index();
			}

		}
	} 

}
