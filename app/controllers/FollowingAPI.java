package controllers;

import groovy.ui.text.FindReplaceUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import models.Follow;
import models.Tweet;
import models.User;
import play.Logger;
import play.mvc.Controller;
import utils.JsonParsers; 

import com.google.gson.JsonElement;

public class FollowingAPI extends Controller
{
	/**
	 * retrieve all tweets
	 */
	public static void followings(String userId)
	{
		User user = User.findById(userId);
		if(user == null)
		{
			notFound();
		}
		else
		{
			List<Follow> followings = user.Following;
			renderJSON(JsonParsers.follow2Json(followings));
		}
	}


	public static void following(String id)
	{
		Follow following = Follow.findById(id);  
		if (following == null)
		{
			notFound();
		}
		else
		{
			renderJSON(JsonParsers.follow2Json(following));
		}
	}

	public static void follow(String userId, JsonElement body)
	{
		User existinguser = User.findById(userId);
		Logger.info("User is found" + existinguser.firstName + " " + existinguser.uuid);
		Follow follow = JsonParsers.json2Follow(body.toString());
		//follow.save();

		//List<Follow> FollowList =  Follow.findAll();
		//List<Follow> newFollowList = new ArrayList<Follow>(existinguser.Following);
		for(Follow usercheck:existinguser.Following)
		{
			Logger.info("User 1 usercheck.targetUser" + usercheck.targetUser);
			Logger.info("User 2 follow.targetUser" + follow.targetUser);


			if ((usercheck.targetUser.equals(follow.targetUser)) || (usercheck.sourceUser.equals(follow.targetUser)))
			{
				Logger.info("User 3 usercheck.targetUser" + usercheck.targetUser);
				Logger.info("User 4 follow.targetUser" + follow.targetUser);
				Logger.info("User 5 usercheck.sourceUser" + usercheck.sourceUser);
				Logger.info("User 6 follow.targetUser" + follow.targetUser);
				Logger.info("ALREADY EXISTS");
				follow.delete();
				notFound();

			}
			
		}
		Logger.info("Saved user following " + follow);

		
		follow.save();
		renderJSON(JsonParsers.follow2Json(follow));
	}



	public static void unfollow(String Followid)
	{
		Follow follow = Follow.findById(Followid);
		if (follow == null)
		{
			notFound();
		}
		else
		{
			follow.delete();
			renderText("success");
		}
	}
	public static void followTweet(String userId)
	{
		User user = User.findById(userId);
		Logger.info("Tweet ctrler : user is " + user.email);
		List<Tweet> tweets = Tweet.findAll();
		List<Follow> followings = Follow.findAll();

		List<Tweet> filteredtweets = new ArrayList();
		for (Follow TweetUser : followings)
		{
			if (TweetUser.sourceUser == user)
			{
				Logger.info(TweetUser.targetUser.firstName + " " + user.firstName);
				for (Tweet onlyfollowedTweets : tweets)
				{
					if (onlyfollowedTweets.from == TweetUser.targetUser)
					{
						filteredtweets.add(onlyfollowedTweets);
					}
				}
			}

		}
		renderJSON(JsonParsers.tweet2Json(filteredtweets));

	}

	public static boolean checkExistingUser(Follow follow)
	{
		List<Follow> users = Follow.findAll();

		for (Follow usercheck:users)
		{
			if (usercheck.targetUser.equals(follow.targetUser))
			{
				return true;
			}
		}
		Logger.info("false ", follow.sourceUser.toString() + follow.targetUser.email.toString());
		return false;
	}

}

