package controllers;

import play.*;
import play.mvc.*;
import groovy.ui.text.FindReplaceUtility;

import java.text.SimpleDateFormat;
import java.util.*;

import models.*;

public class TimeLineController extends Controller
{
	public  String uuid; //mandates use of getter method getId()

	public static void index()
	{
		User user = Accounts.getCurrentUser();
		Logger.info("Tweet ctrler : user is " + user.email);
		List<Tweet> tweets = Tweet.findAll();
		List<Tweet> alltweets = Tweet.findAll();
		List<Follow> followings = Follow.findAll();
		
		List<Tweet> filteredtweets = new ArrayList();
			for (Follow TweetUser : followings)
			{
				if (TweetUser.sourceUser == user)
				{
					Logger.info(TweetUser.targetUser.firstName + " " + user.firstName);
					for (Tweet onlyfollowedTweets : tweets)
					{
							if (onlyfollowedTweets.from == TweetUser.targetUser)
							{
								filteredtweets.add(onlyfollowedTweets);
							}
					}
				}
			}
		
		List<Tweet> mytweets = Tweet.findAll();
		List<Tweet> mynewtweets = new ArrayList();
		for (Tweet onlymyTweets : mytweets)
		{
				if (onlymyTweets.from == user)
				{
					mynewtweets.add(onlymyTweets);
				}
		}

		render(user, alltweets, tweets, filteredtweets, mynewtweets);
	}


}
/*	public static void unfollow(String uuid)
	{
		User user = Accounts.getCurrentUser();
		if (user == null)
		{
			Logger.info("You are Not Logged in ..... Redirecting");
			Accounts.signup();
		}
		else
		{

			User unFollowuser = User.findById(uuid);
			//User me = User.findById(Long.parseLong(userId));
			if (user != unFollowuser)
			{
				user.unfriend(unFollowuser);
				Logger.info("Dropping " + unFollowuser.firstName);
				index();
				}
			else
			{
				index();
				}

		}
	} 
*/
