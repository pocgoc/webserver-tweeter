package controllers;

import play.*;
import play.mvc.*;
import utils.JsonParsers;

import java.text.SimpleDateFormat;
import java.util.*;

import models.*;

public class TweetController extends Controller
{
	  public  String uuid; //mandates use of getter method getId()

  public static void index()
  {
    User user = Accounts.getCurrentUser();
    Logger.info("Tweet ctrler : user is " + user.email);
    List<Tweet> tweets = user.tweets;
    render(user, tweets);
  }
  
public static void viewtweet(String id)
  {
	User user = Accounts.getCurrentUser();
    Logger.info("Tweet id " + id);
    Tweet tweet = Tweet.findById(id);
    Logger.info("Tweet " + tweet);

    render(tweet);
  }
  
  public static void editMyTweet(String id, String userId, String message)
	{
		User user = Accounts.getCurrentUser();
	    User existinguser = User.findById(userId);

		if (user == existinguser)
		{
	    Tweet tweet = Tweet.findById(id);

		tweet.id = tweet.id;
		tweet.message = message;
		tweet.datestamp = tweet.getDateString();
		tweet.geolocation = tweet.geolocation;
		tweet.from = tweet.from;
		Logger.info(tweet.id + " " + tweet.message + " " + tweet.datestamp);
		tweet.save();
		renderReport();
		}
		
		
		else
		{
		index();
		}
	}
  public static void editTweet(String id, String userId, String message,String datestamp)
	{
		User user = Accounts.getCurrentUser();
	    User existinguser = User.findById(userId);

		if (user == existinguser)
		{
	    Tweet tweet = Tweet.findById(id);

	    render(tweet);
		}
		else
		{
		index();
		}
	}


  public static void tweet(String id, String message, String datestamp, String geolocation )
  {
	  
	User user = Accounts.getCurrentUser();
	Logger.info("ID " + user.uuid);
	Logger.info("ID 2 " + user);

    Logger.info("Message " + message);
    Tweet tweet = new Tweet(id, message, datestamp, user, geolocation);
    Logger.info("ID OF TWEET " + id + " MESSAGE " + message + " DATE " + datestamp + " USER ID " + user);
    user.tweets.add(tweet);
    user.save();
    index();
  }


  public static void renderReport()
  {
    User user = Accounts.getCurrentUser();
    List<Tweet> tweets = Tweet.findAll();
    render(tweets);
  }
  

}
