package controllers;

import groovy.ui.text.FindReplaceUtility;

import java.util.List;
import java.util.Random;

import models.Tweet;
import models.User;
import play.Logger;
import play.mvc.Controller;
import utils.JsonParsers; 

import com.google.gson.JsonElement;

public class TweetsAPI extends Controller
{
  /**
   * retrieve all tweets
   */
  public static void tweets(String userId)
  {
    User user = User.findById(userId);
    if(user == null)
    {
      notFound();
    }
    else
    {
      List<Tweet> tweets = user.tweets;
      renderJSON(JsonParsers.tweet2Json(tweets));
    }
  }
  public static void allTweets()
  {

	  List<Tweet> tweets = Tweet.findAll();
	  renderJSON(JsonParsers.tweet2Json(tweets));
  }


  public static void tweet(String id)
  {
    Tweet tweet = Tweet.findById(id);  
    if (tweet == null)
    {
      notFound();
    }
    else
    {
      renderJSON(JsonParsers.tweet2Json(tweet));
    }
  }

  public static void createTweet(String userId, JsonElement body)
  {
    Tweet tweet = JsonParsers.json2Tweet(body.toString());
    User user = User.findById(userId);
    user.tweets.add(tweet);
  tweet.from = user;
    tweet.save();
    renderJSON(JsonParsers.tweet2Json(tweet));
  }

  public static void editTweet(String userId, String id, JsonElement body)
  {
/*	  User existinguser = User.findById(userId);
	    User user = Accounts.getCurrentUser();

		if (user == existinguser)
		{
    Tweet tweet = JsonParsers.json2Tweet(body.toString());
    Tweet Oldtweet = Tweet.findById(id);
user.tweets.remove(Oldtweet);
    user.tweets.add(tweet);
  tweet.from = user;
    tweet.save();
    renderJSON(JsonParsers.tweet2Json(tweet));
		}
		else
		{
		      notFound();
		}
		}*/
	  User existinguser = User.findById(userId);
	    User user = User.findById(userId);

		if (user == existinguser)
		{
	    Logger.info("Editing Tweet ");
	    Logger.info("User " + user);
  Tweet tweet = JsonParsers.json2Tweet(body.toString());
  Logger.info("User " + tweet);
  Tweet Oldtweet = Tweet.findById(id);
  Logger.info("User " + Oldtweet);
  Oldtweet.delete();
  
  user.tweets.add(tweet);
  tweet.from = user;
  Logger.info("User " + tweet.from);
  tweet.save();
  renderJSON(JsonParsers.tweet2Json(tweet));
		}
		else
		{
			notFound();
			
			}
  }
  
  
  public static void deleteTweet(String id)
  {
    Tweet tweet = Tweet.findById(id);
    if (tweet == null)
    {
      notFound();
    }
    else
    {
      tweet.delete();
      renderText("success");
    }
  }

  public static void deleteAllTweets()
  {
    Tweet.deleteAll();
    renderText("success");
  }  

}
/*package controllers;

import play.*;
import play.mvc.*;
import utils.JsonParsers;

import java.util.*;

import com.google.gson.JsonElement;

import models.*;

public class TweetsAPI extends Controller
{
  public static void tweets()
  {
    User user = User.findById(userId);
    List<Tweet> tweets = findall tweets;
    renderText(JsonParsers.tweet2Json(tweets));
	  List<Tweet> tweets = Tweet.findAll();
	  renderJSON(JsonParsers.tweet2Json(tweets));
  }
  
  public static void tweet (String uuid, Long id)
  {
    User user = User.findById(uuid);
    Tweet tweet = Tweet.findById(id);
    if (user.tweets.contains(tweet))
    { 
      renderJSON (JsonParsers.tweet2Json(tweet));
    }
    else
    {
      badRequest();
    }
  }
  

  public static void createTweet(JsonElement body)
  {
    Tweet tweet = JsonParsers.json2Tweet(body.toString());
    tweet.save();
    renderJSON(JsonParsers.tweet2Json(tweet));
  }
  
  public static void createTweet(String uuid, JsonElement body)
  {
    User user = User.findById(uuid);
    Tweet tweet = JsonParsers.json2Tweet(body.toString());
    Tweet newTweet = new Tweet (tweet.TweetId, tweet.message, tweet.datestamp, tweet.from);
    user.tweets.add(newTweet);
    user.save();
    renderJSON (JsonParsers.tweet2Json(newTweet));
  }  
  public static void deleteUserTweet(Long userId, Long id)
  {
    User user = User.findById(userId);
    Tweet tweet = Tweet.findById(id);
    if (!user.tweets.contains(tweet))
    { 
      notFound();
    }
    else
    {
      user.tweets.remove(tweet);
      user.save();
      tweet.delete();
      ok();
    }
  } 
  public static void deleteTweet(String id)
  {
    Tweet tweet = Tweet.findById(id);
    if (tweet == null)
    {
      notFound();
    }
    else
    {
      tweet.delete();
      renderText("success");
    }
  }
}
*/