package models;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.jpa.GenericModel;
import play.db.jpa.Model;

@Entity
public class Follow extends GenericModel
{
	@Id
	@Column(name="id")
	public String Followid;
	@ManyToOne()
	public User sourceUser;

	@ManyToOne()
	public User targetUser;


	public Follow(String Followid, User sourceUser, User targetUser)
	{
	    this.Followid   =  UUID.randomUUID().toString();
		this.sourceUser = sourceUser;
		this.targetUser = targetUser;
	}

	
	
}