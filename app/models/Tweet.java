package models;

import java.text.DateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ForeignKey;

import play.db.jpa.GenericModel;

@Entity
public class Tweet extends GenericModel
{
  @Id
  @Column(name="id")
  public String id;
  public String message;
  public String datestamp;
  public double  zoom       ;//zoom level of accompanying map
public String geolocation;
  
  @ManyToOne
  public User from;
  
  public Tweet(String id, String message, String datestamp, User from, String geolocation)
  {
    this.id       =  UUID.randomUUID().toString();
    this.message    = message;
    this.datestamp  = setDate();
    this.from = from;
    this.geolocation 	= "52.253456,-7.187162";
    this.zoom        = 16.0f;
  }

 public String getDateString()
 {
   return datestamp;
 }


 private String setDate()
 {
   DateFormat df = DateFormat.getDateTimeInstance();
   df.setTimeZone(TimeZone.getTimeZone("UTC"));
   String formattedDate = df.format(new Date());
   return formattedDate;
 }
}
