function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        log("Geolocation is not supported by this browser.")
    }
}
function showPosition(position) {
    log("Latitude: " + position.coords.latitude + 
    "Longitude: " + position.coords.longitude)
}