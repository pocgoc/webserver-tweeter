function initialize() {
	log("Hello")
    var map;
    var marker;
	log("Hello")
    var array = ($('#geolocation').val()).split(','), a = array[0], b = array[1], c = array[2];
	log(array)
    var inputLongit = Number(b);
    var inputLatit = Number(a);

    var latlng = new google.maps.LatLng((inputLatit), (inputLongit));
    var mapOptions = {
        zoom : 16,
        center : new google.maps.LatLng((inputLatit), (inputLongit)),
        mapTypeId : google.maps.MapTypeId.ROADMAP
    };
    
    log(inputLongit)
    log(inputLatit)
    var mapDiv = document.getElementById('map_canvas');
    map = new google.maps.Map(mapDiv,mapOptions);
    mapDiv.style.width = '730px';
    mapDiv.style.height = '600px';
    // place a marker
        marker = new google.maps.Marker({
        map : map,
        draggable : false,
        position : latlng,
        title : "Drag and drop on your property!"
    });
    // To add the marker to the map, call setMap();
    marker.setMap(map); 
    //marker listener populates hidden fields ondragend
    google.maps.event.addListener(marker, 'dragend', function() {
        var latLng = marker.getPosition();
        var latitude = latLng.lat();
        var longitude = latLng.lng();
        //publish lat long in geolocation control in html page
        $("#lng").val(longitude);
        $("#lat").val(latitude);
        //update the new marker position
        map.setCenter(latLng);
      });
}
google.maps.event.addDomListener(window, 'load', initialize);
